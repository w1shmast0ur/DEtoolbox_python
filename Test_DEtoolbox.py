import unittest
from DEtoolbox import *
import numpy as np


class Test_DEtoolbox(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        init_bounds = np.array([[0, 1], [-10, 10], [1, -1]])
        cost_function = lambda pop, data: (np.random.uniform(size=(pop.shape[0], 1)), data)
        pop_function = lambda pop: np.random.uniform(size=(pop.shape[0])) > 0.5
        size_of_pop = 50
        F = 0.6
        CR = 0.9
        mutation_type = MutationType.bst  # cls, bst, cr_t_bst
        scale_factor_type = ScaleFactorType.mix  # cls, dthr1, dthr2, jttr, mix
        JP = 0.3
        F_range = np.array([0.55, 1.1])

        cls.data = "test"
        cls.DEparams = DEparams(init_bounds, cost_function, size_of_pop, F, CR, JP, mutation_type, scale_factor_type,
                                 F_range, pop_function)

    def test_DEparams(self):
        expected_init_bounds = np.array([[0, 1], [-10, 10], [-1, 1]])
        expected_D = expected_init_bounds.shape[0]
        my_DEparams = self.DEparams
        self.assertTrue((my_DEparams.init_bounds == expected_init_bounds).all())  # check correctness of init_bounds
        self.assertEqual(my_DEparams.D, expected_D)  # check the problem's dimension

    def test_DEruns(self):
        my_DEparams = self.DEparams
        my_DErun = DErun(my_DEparams)
        SoP = my_DEparams.size_of_pop
        init_bounds = my_DEparams.init_bounds
        D = my_DEparams.D
        data = self.data

        my_DErun.initialisation(data)

        self.assertEqual(my_DErun.pop_full.shape,
                         (my_DEparams.size_of_pop, my_DEparams.D + 1))  # check the population size
        self.assertTrue((my_DErun.pop >= np.outer(np.ones(SoP), init_bounds[:, 0])).all())  # check the lower boundary
        self.assertTrue((my_DErun.pop <= np.outer(np.ones(SoP), init_bounds[:, 1])).all())  # check the upper boundary

        my_DErun.generation_jump(data)
        self.assertEqual(my_DErun.pop_full.shape, (
        my_DEparams.size_of_pop, my_DEparams.D + 1))  # check the population size after generation jump

        my_DErun.mutation()
        self.assertEqual(my_DErun.mutant.shape,
                         (my_DEparams.size_of_pop, my_DEparams.D))  # check the population size after mutation

        my_DErun.crossover()
        self.assertEqual(my_DErun.trial.shape,
                         (my_DEparams.size_of_pop, my_DEparams.D))  # check the population size after crossover

        my_DErun.selection(data)
        self.assertEqual(my_DErun.pop_full.shape,
                         (my_DEparams.size_of_pop, my_DEparams.D + 1))  # check the population size after mutation


if __name__ == '__main__':
    unittest.main()
