import numpy as np
from enum import Enum, unique


# from scipy import io as sio


@unique
class MutationType(Enum):
    """ Set of possible mutation types fro DE. """
    cls = 1
    bst = 2
    cr_t_bst = 3


@unique
class ScaleFactorType(Enum):
    """ Set of possible choice of scale factor for DE. """
    cls = 1
    dthr1 = 2
    dthr2 = 3
    jttr = 4
    mix = 5


class DEparams(object):
    """ Contains parameters of DE run. 
    
    Attributes:
        init_bounds (ndarray, shape (D,2), 
            where N is number of params to estimate ): 
            Initial bounds for the problem; 
        cost_function (func(pop,data)->(cost,data_output) ): 
            Cost function used to compare the elements of population;
        size_of_pop (int, optional): Size of population;
        F (float, optional): Scale factor;
        CR (float, optional): Crossover probability;
        JP (float, optional): Generation jump probability;
        mutation_type (MutationType, optional): Mutation type;
        scale_factor (ScaleFactorType, optional): Scale factor type;
        F_range (ndarray, shape (1,2), optional): 
            Range for possible values of scale factor 
            (used if scale factor type is not 'cls');
        pop_function (func(pop)->ndarray (D,) dtype=bool, optional ): 
            Population feasibility function
        jttr (float, optional): 
            Factor used in mutation schemes with scale factor type 'jttr' and 'mix' 
    """

    def __init__(self, init_bounds, cost_function, size_of_pop=None, F=0.5, CR=0.9, JP=0.0,
                 mutation_type=MutationType.cls, scale_factor_type=ScaleFactorType.cls,
                 F_range=[0.5, 1], pop_function=None, Jttr=0.01, save_history=False):
        self.init_bounds = init_bounds
        # Correct the initial bounds if necessary
        for row in self.init_bounds:
            if row[0] > row[1]:
                temp = row[0]
                row[0] = row[1]
                row[1] = temp
        self.cost_function = cost_function

        self.D = len(init_bounds)
        if size_of_pop is None:
            self.size_of_pop = 10 * self.D
        else:
            self.size_of_pop = size_of_pop
        self.F = F
        self.CR = CR
        self.mutation_type = mutation_type
        self.scale_factor_type = scale_factor_type
        self.JP = JP
        self.F_range = F_range
        self.Jttr = Jttr
        if pop_function is None:
            self.pop_function = lambda pop: np.ones((self.size_of_pop,), dtype=bool)
        else:
            self.pop_function = pop_function
        self.save_history = save_history


class DErun(object):
    """ Contains necessary routines for DE algorithm.
    
    Attributes:
        de_params (:obj:`DEparams`): DE settings
    """

    def __init__(self, de_params):
        self._de_params = de_params
        self._mutant = None
        self._trial = None
        if self.de_params.save_history:
            self._pop_full = []
        else:
            self._pop_full = None

    @property
    def de_params(self):
        return self._de_params

    @property
    def pop_full(self):
        """ Full population.

        :obj:`ndarray` of shape (N,D+1): full population, i.e. both population and corresponding cost-function values
        """
        if self.de_params.save_history:
            return self._pop_full[-1]
        else:
            return self._pop_full

    @pop_full.setter
    def pop_full(self, value):
        if self.de_params.save_history:
            self._pop_full.append(value)
        else:
            self._pop_full = value

    @property
    def pop(self):
        """ Population only.
        
        :obj:`ndarray` of shape (N,D): population only, i.e. only parameters without cost-function values
        """
        return self.pop_full[:, :-1]

    @property
    def weights(self):
        """ Population weights. 
        
        :obj:`ndarray` of shape (N,)
        """
        return self.pop_full[:, -1]

    @property
    def mutant(self):
        """ Mutant population. 
        
        :obj:`ndarray` of shape (N,D)
        """
        return self._mutant

    @mutant.setter
    def mutant(self, value):
        self._mutant = value

    @property
    def trial(self):
        """ Trial population.
        
        :obj:`ndarray` of shape (N,D): Full trial population prepared for selection step
        """
        return self._trial

    @trial.setter
    def trial(self, value):
        self._trial = value

    def initialisation(self, data=None):
        """ Initialize the first generation. 
        
        Devoted to initialize the first generation by uniform random sampling the parameters within given initial bounds.
        
        Store the initialize population into the class instance.

        Returns initialized population and some output data.
            
        """
        SoP = self.de_params.size_of_pop
        init_bounds = self.de_params.init_bounds
        D = self.de_params.D
        func = self.de_params.cost_function
        # Uniform sampling
        pop = np.outer(np.ones(SoP), init_bounds[:, 0]) + \
              np.outer(np.ones(SoP), init_bounds[:, 1] - init_bounds[:, 0]) * np.random.uniform(size=[SoP, D])
        cost, data_output = func(pop, data)
        self.pop_full = np.concatenate((pop, cost), axis=1)
        return self.pop_full, data_output

    def step(self, data=None):
        """ Make 'generation jump' or one DE step.
       
        Make the 'generation jump' with given probability, otherwise make the usual DE steps.

        Return the new generation and some output data.
        """
        JP = self.de_params.JP
        if np.random.uniform() < JP:
            old_ind = None
            pop_full, data_output = self.generation_jump(data)
        else:
            pop_full, data_output, old_ind = self.classic_step(data)
        return pop_full, data_output, old_ind

    def generation_jump(self, data=None):
        """ Make 'generation jump'.
       
        Obtain the opposite to the current generation, discard the unfeasible elements, apply the cost function and take N best elements
        out of both current and opposite generation. 

        Store the new generation into the class instance.

        Return the new generation and some output data.
        """
        SoP = self.de_params.size_of_pop
        init_bounds = self.de_params.init_bounds
        D = self.de_params.D
        func = self.de_params.cost_function
        cur_pop = self.pop
        # obtain the boundaries of the population
        mins = np.amin(cur_pop, axis=0)
        maxs = np.amax(cur_pop, axis=0)
        # produce the opposite population
        op_pop = np.tile(mins, (SoP, 1)) + np.tile(maxs, (SoP, 1)) - cur_pop
        # discard unfeasible
        feasible = self.de_params.pop_function(op_pop)
        op_pop = op_pop[feasible, :]
        # calculate weights
        cost, data_output = func(op_pop, data)
        op_pop_full = np.concatenate((op_pop, cost), axis=1)
        # select the best elements
        cur_pop_full = np.concatenate((self.pop_full, op_pop_full), axis=0)
        ind = np.asarray(np.argsort(cur_pop_full[:, -1], axis=0)).flatten()
        new_pop_full = cur_pop_full[ind[:SoP], :]
        self.pop_full = new_pop_full[np.random.permutation(SoP), :]
        return self.pop_full, data_output

    def classic_step(self, data=None):
        """ Make one full DE step, i.e. mutation, crossover and selection.
       
        Usual DE step which consists of mutation, crossover and selection.
        
        Return the new generation and some output data.    
        """

        self._mutant = None
        self._trial = None
        self.mutation()
        self.crossover()
        _, data_output, old_ind = self.selection(data)
        return self.pop_full, data_output, old_ind

    def mutation(self):
        """ Make mutation. 
        
        Construct the mutant vector. Several mutation schemes are available.

        Store the mutant population into the class instance.

        Return the mutant population.
        """
        SoP = self.de_params.size_of_pop
        D = self.de_params.D
        scale_factor_type = self.de_params.scale_factor_type
        mutation_type = self.de_params.mutation_type
        weights = self.weights
        pop = self.pop

        F_l = self.de_params.F_range[0]
        F_u = self.de_params.F_range[1]

        if scale_factor_type == ScaleFactorType.cls:
            # Single fixed scale factor for each element and dimension
            F = np.tile(self.de_params.F, pop.shape)
        elif scale_factor_type == ScaleFactorType.dthr1:
            # Single random scale factor for each element and dimension drawn
            # from [F_u,F_l]
            F_g = F_l + np.random.uniform() * (F_u - F_l)
            F = np.tile(F_g, pop.shape)
        elif scale_factor_type == ScaleFactorType.dthr2:
            # Unique scale factor for each element of population drawn drom
            # [F_u, F_l]
            F = np.tile(np.tile(F_l, (SoP, 1)) + (F_u - F_l) * np.random.uniform(size=(SoP, 1)), (1, D))
        elif scale_factor_type == ScaleFactorType.jttr:
            # Unique scale factor for each dimension which is fluctuation
            # around given F
            delta = self.de_params.Jttr
            F = self.de_params.F * (np.ones(pop.shape) +
                                    delta * (np.random.uniform(size=pop.shape) - 0.5 * np.ones(pop.shape)))
        elif scale_factor_type == ScaleFactorType.mix:
            # Combination of 2 previous options
            delta = self.de_params.Jttr
            F_g = F_l + np.random.uniform(size=(SoP, 1)) * (F_u - F_l)
            F = F_g * (np.ones(pop.shape) + delta * (np.random.uniform(size=pop.shape) - 0.5 * np.ones(pop.shape)))

        # prepare the indexes for mutation which are mutually different to each
        # other as well as to the number of target vector
        diag_ind = np.logical_not(np.eye(SoP, dtype=bool))
        inds = np.tile(np.arange(SoP, dtype=int), (SoP, 1))
        ind_mutation = np.apply_along_axis(np.random.permutation, 1, inds[diag_ind].reshape(SoP, SoP - 1))

        if mutation_type == MutationType.cls:
            # random base vector plus scale difference of two others
            mutant = pop[ind_mutation[:, 0], :] + F * (pop[ind_mutation[:, 1], :] - pop[ind_mutation[:, 2], :])
        elif mutation_type == MutationType.bst:
            # best vector as a base plus scale difference of two random others
            ind_bst = np.argmin(weights) * np.ones((SoP, 1), dtype=int)
            ind_mutation = np.concatenate((ind_bst, ind_mutation), axis=1)
            mutant = pop[ind_mutation[:, 0], :] + F * (pop[ind_mutation[:, 1], :] - pop[ind_mutation[:, 2], :])
        elif mutation_type == MutationType.cr_t_bst:
            # combination of two previous cases
            ind_bst = np.argmin(weights) * np.ones((SoP, 1), dtype=int)
            ind_mutation = np.concatenate((ind_bst, ind_mutation), axis=1)
            mutant = pop[ind_mutation[:, 0], :] + F * \
                                                  (pop[ind_mutation[:, 0], :] - pop[ind_mutation[:, 1], :] + \
                                                   pop[ind_mutation[:, 2], :] - pop[ind_mutation[:, 3], :])

        # discard the unfeasible by preserving the corresponding current
        feasible = self.de_params.pop_function(mutant)
        mutant[np.logical_not(feasible), :] = self.pop[np.logical_not(feasible), :]

        self.mutant = mutant
        return self.mutant

    def crossover(self):
        """ Make crossover. 
        
        Make crossover between mutant and current populations, enforcing the difference at least in a one dimension.

        Store the trial population into the class instance.

        Return the trial population and some output data.
        """
        SoP = self.de_params.size_of_pop
        D = self.de_params.D
        CR = self.de_params.CR
        mutant = self.mutation() if self.mutant is None else self.mutant
        pop = self.pop

        # prepare the probabilities
        r = np.random.uniform(size=mutant.shape)
        ind = np.random.randint(0, D, size=(SoP,))
        # enforce the difference at least in one dimension
        r[np.arange(SoP), ind] = 0
        # check the probabilities of crossover
        ind_crossover = r > CR
        # make the trial population
        trial = mutant.copy()
        trial[ind_crossover] = pop[ind_crossover]
        # discard unfeasible by preserving the current
        feasible = self.de_params.pop_function(trial)
        trial[np.logical_not(feasible), :] = pop[np.logical_not(feasible), :]
        self.trial = trial
        return trial

    def selection(self, data):
        """ Make selection.
       
        'Survival the fittest' selection between trial and current populations.

        Store the new generation into the class instance.

        Return the new generation.
        """
        func = self.de_params.cost_function
        trial = self.crossover() if self.trial is None else self.trial
        cost, data_output = func(trial, data)
        trial_full = np.concatenate((trial, cost), axis=1)
        ind = trial_full[:, -1] > self.pop_full[:, -1]
        new_pop_full = trial_full.copy()
        new_pop_full[ind, :] = self.pop_full[ind, :]
        self.pop_full = new_pop_full
        return self.pop_full, data_output, ind

    def recalculation(self, data=None):
        """ Make recalculation step.
        
        During this step, we simply update the weights for the current population with respect to current data, not population itself.

        Store the new generation into the class instance.

        Return the new generation and some output data.
        """
        pop = self.pop
        func = self.de_params.cost_function
        cost, data_output = func(pop, data)
        self.pop_full = np.concatenate((pop, cost), axis=1)
        return self.pop_full, data_output

    def repeat_generation(self):
        self.pop_full = np.copy(self.pop_full)

    def get_history(self):
        history = np.dstack(self._pop_full)
        return history[:, :-1, :]

    # def save_history_mat(self, file_name):
    #     history = {'history': self.get_history().astype(float)}
    #     sio.savemat(file_name, history, False, '5')
